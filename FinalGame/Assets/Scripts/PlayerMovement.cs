﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public Text livesText;

    public TicketCounter ticketCounter;

    public Transform destination;

    public GameObject player;

    public float moveSpeed;
    public CharacterController controller;
    public Rigidbody playerRb;
    public float jumpForce = 30;
    public float trampForce = 1000f;
    private float currentJumpForce;
    public float speed = 6f;
    public int lives = 6;

    public Animator anim;

    private Vector3 moveDirection;
    public float gravityScale;

    public int ticket = 0;

    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {

        //respawns if player falls
        if(player.transform.position.y < -8) {

            controller.enabled = false;
            //Debug.Log("Falling");
            this.player.transform.position = destination.position;
            controller.enabled = true;

            livesText.text = "Lives :" + lives --;
        }

     //theRB.velocity = new Vector3(Input.GetAxis("Horizontal") * moveSpeed, theRB.velocity.y, Input.GetAxis("Vertical") * moveSpeed);

     /*if(Input.GetButtonDown("Jump"))
     {
        theRB.velocity = new Vector3(theRB.velocity.x, jumpForce, theRB.velocity.z);
     } */

     //moveDirection = new Vector3(Input.GetAxis("Horizontal") * moveSpeed, moveDirection.y, Input.GetAxis("Vertical") * moveSpeed);
    float yStore = moveDirection.y;
    moveDirection = (transform.forward * Input.GetAxis("Vertical")) + (transform.right * Input.GetAxis("Horizontal"));
    moveDirection = moveDirection.normalized * moveSpeed;
    moveDirection.y = yStore;

     if(controller.isGrounded)
     {
        moveDirection.y = 0f;
        if (Input.GetButtonDown("Jump"))
        {
            moveDirection.y = jumpForce;
        }
     }



     moveDirection.y = moveDirection.y + (Physics.gravity.y * gravityScale * Time.deltaTime);
     moveDirection += Vector3.down * Time.deltaTime * 75;
     controller.Move(moveDirection * Time.deltaTime);

     anim.SetBool("isGrounded", controller.isGrounded);
     anim.SetFloat("Speed", (Mathf.Abs(Input.GetAxis("Vertical")) + Mathf.Abs(Input.GetAxis("Horizontal"))));
    }

    void FixedUpdate()
    {
        if (lives <= -1)
        {
            FindObjectOfType<GameManager>().EndGame();
        }
    }


    public void IncreaseTicket(int add)
    {
        ticketCounter.tickets +=add;
    }
}
