﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TicketCounter : MonoBehaviour
{
    public int tickets = 0;
    public Text ticketText;

    void Start()
    {

    }

    void Update()
    {
        ticketText.text = "            : " + tickets;


        if (tickets >= 8)
        {
            FindObjectOfType<GameManager>().WinGame();
        }
    }


}
