﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    bool gameEnd = false;

    bool winGame = false;

    void Start()
    {

    }


    public void EndGame()
    {
        if (gameEnd == false)
        {
            gameEnd = true;
            Debug.Log("Game Over");
            Restart();
        }
    }
    void Restart ()
    {
        SceneManager.LoadScene("GameOver");
    }

    public void WinGame()
    {
        if (winGame == false)
        {
            winGame = true;
            Debug.Log("Game Over");
            YouWin();
        }
    }

    void YouWin ()
    {
        SceneManager.LoadScene("YouWin");
    }
}
